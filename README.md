#skykingkong-common-oauth2
> 设置应用成为OAuth2 Client & ResourceServer，使用FeignClient访问HTTP MicroService

* 在项目中引入依赖

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-oauth2</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-security</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.security.oauth</groupId>
    <artifactId>spring-security-oauth2</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-feign</artifactId>
</dependency>
<dependency>
    <groupId>com.jkgj.skykingkong</groupId>
    <artifactId>skykingkong-common-oauth2</artifactId>
    <version>1.4</version>
</dependency>
```
* 在Application类上加上配置注解：

```java
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableCustomUserInfoTokenService //  使用自定义UserInfoTokenService获取当前已认证用户
@EnableOAuth2Client
@EnableFeignClients
@EnableCustomOAuth2FeignRequestInterceptor // 使用自定义OAuth2FeignRequestInterceptor自动添加Authorization头
```
* 自定义安全访问配置

```java
@SpringBootApplication
@EnableDiscoveryClient
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableCustomUserInfoTokenService
@EnableOAuth2Client
@EnableFeignClients
@EnableCustomOAuth2FeignRequestInterceptor
@Configuration
public class DemoApplication extends ResourceServerConfigurerAdapter {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        // 配置授权
        http.authorizeRequests()
                .antMatchers("/", "/demo").permitAll()
                .anyRequest().authenticated();
    }
}
```
* 获取登录用户信息

```java
action(@AuthenticationPrincipal UserPrincipal userPrincipal)
```